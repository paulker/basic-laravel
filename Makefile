APP=test-laravel
NETWORK=$(APP)

COMPOSE=docker-compose

# services
PHP=php
NGINX=web
DB=database

##
## Docker
## -------
##

build: ## Build docker images (options: app [php])
	$(eval app ?= )
	@$(COMPOSE) build $(app)

up: ## Start the project
	@$(COMPOSE) up -d $(PHP) $(DB) $(NGINX)

stop: ## Stop container (options: app)
	$(eval app ?= )
	$(COMPOSE) stop $(app)

down: ## Down containers and remove orphans
	@$(COMPOSE) down -v --remove-orphans

ps: ## List running containers
	@$(COMPOSE) ps

restart: stop up ## restart the project

exec: ## Open a shell in the application container (options: user [www-data], app [php], cmd [bash])
	$(eval user ?= www-data)
	$(eval app ?= $(PHP))
	$(eval cmd ?= bash)
	@$(COMPOSE) exec --user=$(user) $(app) $(cmd)

logs: ## Display project's logs (options: app [php])
	$(eval app ?= )
	@$(COMPOSE) logs -f $(app)

.PHONY: build up stop down ps restart exec logs

##
## Project
## -------
##

start: build up install ## Build, install project and start containers

install: ## Just install project with all dependencies and setup database
	@$(COMPOSE) run --rm $(PHP) /usr/local/bin/composer install

migration: ## Create migration
	$(COMPOSE) exec $(PHP) php bin/console d:m:diff
	$(COMPOSE) exec $(PHP) php bin/console d:m:m --no-interaction

clean: ## Remove all dependencies (composer)
	rm -rf app/vendor

reset: down clean ## Stop all containers and remove all dependencies (composer)

mysql: ## Connect to database (ssh mode)
	$(COMPOSE) exec $(DB) mysql database -udev -pdev -h $(DB).$(NETWORK)

.PHONY: start install create-db migration clean reset mysql

##
## Tests
## -----
##

phpstan: ## Launches phpstan test
	$(COMPOSE) exec $(PHP) vendor/bin/phpstan analyse -c phpstan.neon -l7 --ansi src

cs-fixer: ## Launches php-cs-fixer test
	$(COMPOSE) exec $(PHP) vendor/bin/php-cs-fixer fix --dry-run --diff

cs-fixer-fix: ## Launches php-cs-fixer fix
	$(COMPOSE) exec $(PHP) vendor/bin/php-cs-fixer fix --no-ansi

phpunit: ## Launches phpunit tests
	rm -fr var/cache/test
	$(COMPOSE) exec $(PHP) ./bin/phpunit "$(path)"

check: phpstan cs-fixer phpunit

.PHONY: phpstan cs-fixer cs-fixer-fix phpunit check

##
## Utils
## -----
##

.DEFAULT_GOAL := help
help: ## List all commands available in Makefile
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.PHONY: help
