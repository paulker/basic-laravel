#!/bin/bash

# look inside /usr/src/app to move source
if [ "$(ls -A /usr/src/app)" ]; then
     cp -r /usr/src/app/. /var/www/html
fi

# Change www-data's uid & guid to be the same as directory in host
# Fix cache problems
: ${WWW_DATA_TESTER_PATH:="/var/www/html"}
: ${WWW_DATA_UID:=`stat -c %u ${WWW_DATA_TESTER_PATH}`}
: ${WWW_DATA_GID:=`stat -c %g ${WWW_DATA_TESTER_PATH}`}

usermod -u ${WWW_DATA_UID} www-data || true
groupmod -g ${WWW_DATA_GID} www-data || true

chown -R www-data:www-data /var/www

set -e

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- php-fpm "$@"
fi

exec "$@"
